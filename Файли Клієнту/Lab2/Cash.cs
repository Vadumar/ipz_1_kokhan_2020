﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Lab2
{
    public partial class Cash : Form
    {
        static int port = 8005; // порт сервера
        static string address = "127.0.0.1"; // адрес сервера

        public void ConnectToSrv(string str)
        {
            IPEndPoint ipPoint = new IPEndPoint(IPAddress.Parse(address), port);
            Socket socket = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
            // подключаемся к удаленному хосту
            socket.Connect(ipPoint);
            string message = str; //Console.ReadLine();
            byte[] data = Encoding.Unicode.GetBytes(message);
            socket.Send(data);
        }


        public Cash()
        {
            InitializeComponent();
            label5.Text = cash.ToString();
            SqlConnection sqlcon = new SqlConnection(@"Data Source=DESKTOP-DK91BTC\MSSQLSERVER01;Initial Catalog=IPZ;Integrated Security=True");
            sqlcon.Open();
            SqlCommand cmd = new SqlCommand("Select Balance.balance from Balance right join Current_Info on Balance.Username = Current_Info.username", sqlcon);//     "Select Current_Info.balance from Current_Info", sqlcon);
            SqlDataReader dataReader1 = cmd.ExecuteReader();
            while (dataReader1.Read())
            {
                label4.Text = dataReader1.GetInt32(0).ToString();
            }
            sqlcon.Close();
        }

        public static int cash = 100000;

        private void button1_Click(object sender, EventArgs e)
        {
            this.DestroyHandle();
            ConnectToSrv("Opperaton cancelled");
        }

        private void button2_Click(object sender, EventArgs e)
        {
            if (Convert.ToInt32(textBox1.Text) <= Convert.ToInt32(label4.Text) & Convert.ToInt32(textBox1.Text) <= Convert.ToInt32(label5.Text))
            {
                textBox2.Visible = true;
                textBox3.Visible = true;
                button3.Visible = true;
                button4.Visible = true;
            }
            else
            {
                MessageBox.Show("Така сума не доступна");
                ConnectToSrv("There was asked for too much money");
            }
        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {
            if (textBox1.Text != "")
            {
                button2.Enabled = true;
            }
        }

        private void textBox3_TextChanged(object sender, EventArgs e)
        {
            if ((textBox3.Text != "Username" & textBox3.Text != "") & (textBox2.Text != "Password" & textBox2.Text != ""))
            {
                button4.Enabled = true;
            }            
        }

        private void textBox2_TextChanged(object sender, EventArgs e)
        {
            if ((textBox3.Text != "Username" & textBox3.Text != "") & (textBox2.Text != "Password" & textBox2.Text != ""))
            {
                button4.Enabled = true;
            }
        }

        private void textBox3_Click(object sender, EventArgs e)
        {
            if (textBox3.Text == "Username")
            {
                textBox3.Text = "";
            }

            if (textBox2.Text == "")
            {
                textBox2.Text = "Password";
            }
        }

        private void textBox2_Click(object sender, EventArgs e)
        {
            if (textBox2.Text == "Password")
            {
                textBox2.Text = "";
            }

            if (textBox3.Text == "")
            {
                textBox3.Text = "Username";
            }
        }

        private void button4_Click(object sender, EventArgs e)
        {

            SqlConnection sqlcon = new SqlConnection(@"Data Source=DESKTOP-DK91BTC\MSSQLSERVER01;Initial Catalog=IPZ;Integrated Security=True");
            string query1 = "Select * from Current_Info where username = '" + textBox3.Text.Trim() + "'";
            SqlDataAdapter sda1 = new SqlDataAdapter(query1, sqlcon);
            DataTable dataTable1 = new DataTable();
            sda1.Fill(dataTable1);
            if (dataTable1.Rows.Count == 1)
            {
                string query = "Select * from User_Info where username = '" + textBox3.Text.Trim() + "' and password = '" + textBox2.Text.Trim() + "'";
                SqlDataAdapter sda = new SqlDataAdapter(query, sqlcon);
                DataTable dataTable = new DataTable();
                sda.Fill(dataTable);
                if (dataTable.Rows.Count == 1)
                {
                    MessageBox.Show("Підтверджено");
                    this.DestroyHandle();
                    ConnectToSrv("Success");

                    cash = cash - Convert.ToInt32(textBox1.Text);

                    string query2 = "Update Balance set Balance = Balance.Balance - " + Convert.ToInt32(textBox1.Text) + " where username = '" + textBox3.Text.Trim() + "'";
                    SqlDataAdapter sda2 = new SqlDataAdapter(query2, sqlcon);
                    DataTable dataTable2 = new DataTable();
                    sda2.Fill(dataTable2);

                    string name = textBox3.Text.Trim().ToString() + ".txt";
                    StreamWriter write_text;  //Класс для записи в файл
                    FileInfo file = new FileInfo(name);
                    write_text = file.AppendText(); //Дописываем инфу в файл, если файла не существует он создастся
                    write_text.WriteLine(DateTime.Now + " Transaction " + textBox3.Text); //Записываем в файл текст из текстового поля
                    write_text.Close(); // Закрываем файл

                }
                else
                {
                    MessageBox.Show("Не вірний логін або пароль");
                    ConnectToSrv("Process failed");
                }
            }

                else
                {
                    MessageBox.Show("Не вірний логін або пароль");
                    ConnectToSrv("Process failed");
                }

                
           

        }

        private void button3_Click(object sender, EventArgs e)
        {
            textBox2.UseSystemPasswordChar = !textBox2.UseSystemPasswordChar;
        }
    }
}
