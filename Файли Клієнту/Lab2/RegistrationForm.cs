﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Lab2
{
    public partial class RegistrationForm : Form
    {
        static int port = 8005; // порт сервера
        static string address = "127.0.0.1"; // адрес сервера

        public void ConnectToSrv(string str)
        {
            IPEndPoint ipPoint = new IPEndPoint(IPAddress.Parse(address), port);
            Socket socket = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
            // подключаемся к удаленному хосту
            //socket.Connect(ipPoint);
            string message = str; //Console.ReadLine();
            byte[] data = Encoding.Unicode.GetBytes(message);
            //socket.Send(data);
        }


        public RegistrationForm()
        {
            InitializeComponent();
            openFileDialog1.Filter = "Text files(*.txt)|*.txt|All files(*.*)|*.*";
            saveFileDialog1.Filter = "Text files(*.txt)|*.txt|All files(*.*)|*.*";
        }

        private void textBox1_Click(object sender, EventArgs e)
        {
            //if (textBox1.Text == "Username")
            //{
            //    textBox1.Text = "";
            //    usernameTextBox.Text = textBox1.Text;
            //}

            //if (textBox2.Text == "")
            //{
            //    textBox2.Text = "Password";
            //}

            //if (textBox3.Text == "")
            //{
            //    textBox3.Text = "Confirm password";
            //    textBox3.UseSystemPasswordChar = false;
            //}
        }

        private void textBox2_Click(object sender, EventArgs e)
        {
            //if (textBox2.Text == "Password")
            //{
            //    textBox2.Text = "";
            //}

            //if (textBox1.Text == "")
            //{
            //    textBox1.Text = "Username";
            //}

            //if (textBox3.Text == "")
            //{
            //    textBox3.Text = "Confirm password";
            //    textBox3.UseSystemPasswordChar = false;
            //}
        }

        private void textBox3_Click(object sender, EventArgs e)
        {
            //textBox3.UseSystemPasswordChar = true;

            //if(textBox3.Text=="Confirm password")
            //{
            //    textBox3.Text = "";
            //}

            //if (textBox2.Text == "")
            //{
            //    textBox2.Text = "Password";
            //}

            //if (textBox1.Text == "")
            //{
            //    textBox1.Text = "Username";
            //}
        }

        private void button1_Click(object sender, EventArgs e)
        {
            this.DestroyHandle();
            ConnectToSrv("Registration cancelled");
        }

        private void textBox3_TextChanged(object sender, EventArgs e)
        {
            //if ((textBox1.Text != "Username" & textBox1.Text != "") & (textBox2.Text != "Password" & textBox2.Text != "")&(textBox3.Text!="Confirm password"&textBox3.Text!=""))
            //{
            //    button2.Enabled = true;
            //}
            //else
            //    button2.Enabled = false;
        }

        private void button2_Click(object sender, EventArgs e)
        {
            SqlConnection sqlcon = new SqlConnection(@"Data Source=DESKTOP-DK91BTC\MSSQLSERVER01;Initial Catalog=IPZ;Integrated Security=True");
            string query = "Select * from User_Info where username = '" + usernameTextBox.Text.Trim() + "'";
            SqlDataAdapter sda = new SqlDataAdapter(query, sqlcon);
            DataTable dataTable = new DataTable();
            //sda.Fill(dataTable);



            if (dataTable.Rows.Count == 0)
            {
                MessageBox.Show("Реєстрація пройшла успішно");
                ConnectToSrv("Success registration. New user '" + usernameTextBox.Text.Trim() + "' added");
                this.DestroyHandle();

                user_InfoBindingSource.EndEdit();
                //user_InfoTableAdapter.Update(dataSet1);

                string query1 = "Insert into Balance(username) Select User_Info.Username from User_Info where User_Info.Username = '" + usernameTextBox.Text.Trim() + "' Update Balance set balance = 0 where Balance.Username = '" + usernameTextBox.Text.Trim() + "'";
                SqlDataAdapter sda1 = new SqlDataAdapter(query1, sqlcon);
                DataTable dataTable1 = new DataTable();
                //sda1.Fill(dataTable1);

                string name = usernameTextBox.Text.Trim().ToString() + ".txt";
                StreamWriter write_text;  //Класс для записи в файл
                FileInfo file = new FileInfo(name);
                write_text = file.AppendText(); //Дописываем инфу в файл, если файла не существует он создастся
                write_text.WriteLine(DateTime.Now + " " + usernameTextBox.Text); //Записываем в файл текст из текстового поля
                write_text.Close(); // Закрываем файл

            }
            else
            {
                MessageBox.Show("Логін зайнято");
                ConnectToSrv("Registration failed");
            }
            //if (textBox1.Text == "Test123" & (textBox2.Text == textBox3.Text))
            //{
            //    MessageBox.Show("Реєстрація пройшла успішно");
            //    this.DestroyHandle();
            //}
            //else
            //    MessageBox.Show("Логін зайнято, або пароль не вірний");
            //user_InfoBindingSource.EndEdit();
            //user_InfoTableAdapter.Update(dataSet1);
        }

        private void user_InfoBindingNavigatorSaveItem_Click(object sender, EventArgs e)
        {
            this.Validate();
            this.user_InfoBindingSource.EndEdit();
            this.tableAdapterManager.UpdateAll(this.dataSet1);

        }

        private void RegistrationForm_Load(object sender, EventArgs e)
        {
            // TODO: This line of code loads data into the 'dataSet1.Balance' table. You can move, or remove it, as needed.
            //this.balanceTableAdapter.Fill(this.dataSet1.Balance);
            // TODO: This line of code loads data into the 'dataSet1.User_Info' table. You can move, or remove it, as needed.
            //this.user_InfoTableAdapter.Fill(this.dataSet1.User_Info);

        }

        private void button3_Click(object sender, EventArgs e)
        {
            user_InfoBindingSource.EndEdit();
            user_InfoTableAdapter.Update(dataSet1);
        }

        private void usernameTextBox_Click(object sender, EventArgs e)
        {

        }

        private void button4_Click(object sender, EventArgs e)
        {
            usernameTextBox.Visible = true;
            button2.Visible = true;
            label2.Visible = true;
            label3.Visible = true;
            usernameTextBox.Enabled = true;
            passwordTextBox.Visible = true;
            user_InfoBindingSource.AddNew();
            ConnectToSrv("Registration started");
        }

        private void passwordTextBox_TextChanged(object sender, EventArgs e)
        {
            if (usernameTextBox.Text != "" & this.Text != "")
                button2.Enabled = true;
        }
    }
}
