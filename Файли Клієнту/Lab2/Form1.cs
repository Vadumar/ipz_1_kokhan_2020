﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Lab2
{
    public partial class Form1 : Form
    {
        static int port = 8005; // порт сервера
        static string address = "127.0.0.1"; // адрес сервера

        
        
        public void ConnectToSrv(string str)
        {
            IPEndPoint ipPoint = new IPEndPoint(IPAddress.Parse(address), port);
            Socket socket = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
            // подключаемся к удаленному хосту
            //socket.Connect(ipPoint);
            string message = str; //Console.ReadLine();
            byte[] data = Encoding.Unicode.GetBytes(message);
            //socket.Send(data);
        }

        public Form1()
        {
            InitializeComponent();
            SqlConnection sqlcon = new SqlConnection(@"Data Source=DESKTOP-DK91BTC\MSSQLSERVER01;Initial Catalog=IPZ;Integrated Security=True");
            string query = "Truncate table Current_info";
            SqlDataAdapter sda = new SqlDataAdapter(query, sqlcon);
            DataTable dataTable = new DataTable();
            //sda.Fill(dataTable);
           // ConnectToSrv("Connected!");
        }

        private void button1_Click(object sender, EventArgs e)
        {
            SqlConnection sqlcon = new SqlConnection(@"Data Source=DESKTOP-DK91BTC\MSSQLSERVER01;Initial Catalog=IPZ;Integrated Security=True");
            string query = "Select * from User_Info where username = '" + textBox1.Text.Trim() + "' and password = '" + textBox2.Text.Trim() + "'";
            SqlDataAdapter sda = new SqlDataAdapter(query, sqlcon);
            DataTable dataTable = new DataTable();
            //sda.Fill(dataTable);

            //SqlConnection sqlcon = new SqlConnection(@"Data Source=DESKTOP-DK91BTC\MSSQLSERVER01;Initial Catalog=IPZ;Integrated Security=True");


            //string query2 = "Select Balance.username, Balance.balance from Balance left join Current_Info on Balance.username = Current_info.username";
            //SqlDataAdapter sda2 = new SqlDataAdapter(query2, sqlcon);
            //DataTable dataTable2 = new DataTable();
            //sda1.Fill(dataTable2);

            if (dataTable.Rows.Count == 1)
            {
                string query1 = "Insert into Current_Info values ( '" + textBox1.Text.Trim() + "', " + 0 + ")";
                //string query1 = "Insert into Current_Info(username, balance) Select User_Info.Username, User_Info.Balance from User_Info where User_Info.Username = '" + textBox1.Text.Trim() + "'";
                SqlDataAdapter sda1 = new SqlDataAdapter(query1, sqlcon);
                DataTable dataTable1 = new DataTable();
                sda1.Fill(dataTable1);

                ConnectToSrv("Login success. User '" + textBox1.Text.Trim() + "' signed in");
                textBox1.Text = "Username";
                textBox2.Text = "Password";
                textBox2.UseSystemPasswordChar = false;
                UserWindow newForm = new UserWindow();
                newForm.Show();                
            }
            else
            {
                MessageBox.Show("Не вірний логін або пароль");
                ConnectToSrv("Login failed");
            }
        }

        private void textBox1_Click(object sender, EventArgs e)
        {
            if(textBox1.Text == "Username")
            {
                textBox1.Text = "";
            }
            
            if (textBox2.Text == "")
            {
                textBox2.Text = "Password";
            }
        }

        private void textBox2_Click(object sender, EventArgs e)
        {
            if (textBox2.Text == "Password")
            {
                textBox2.Text = "";
            }
            
            if (textBox1.Text == "")
            {
                textBox1.Text = "Username";
            }
        }

        private void button3_Click(object sender, EventArgs e)
        {
            textBox2.UseSystemPasswordChar = !textBox2.UseSystemPasswordChar;
        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {
            if ((textBox1.Text != "Username" & textBox1.Text != "") & (textBox2.Text != "Password" & textBox2.Text != ""))
            {
                button1.Enabled = true;
            }
            else
                button1.Enabled = false;
        }

        private void textBox2_TextChanged(object sender, EventArgs e)
        {
            if ((textBox1.Text != "Username" & textBox1.Text != "") & (textBox2.Text != "Password" & textBox2.Text != ""))
            {
                button1.Enabled = true;
            }
            else
                button1.Enabled = false;
        }

        private void button2_Click(object sender, EventArgs e)
        {
            RegistrationForm newForm = new RegistrationForm();
            newForm.Show();

            ConnectToSrv("Registration");
            //user_InfoBindingSource.AddNew();
        }

        private void user_InfoBindingNavigatorSaveItem_Click(object sender, EventArgs e)
        {
            this.Validate();
            this.user_InfoBindingSource.EndEdit();
            this.tableAdapterManager.UpdateAll(this.dataSet1);

        }

        private void Form1_Load(object sender, EventArgs e)
        {
            // TODO: This line of code loads data into the 'dataSet1.User_Info' table. You can move, or remove it, as needed.
            //this.user_InfoTableAdapter.Fill(this.dataSet1.User_Info);

        }
    }
}
