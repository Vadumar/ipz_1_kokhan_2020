﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Lab2
{
    public partial class UserWindow : Form
    {
        static int port = 8005; // порт сервера
        static string address = "127.0.0.1"; // адрес сервера

        public void ConnectToSrv(string str)
        {
            IPEndPoint ipPoint = new IPEndPoint(IPAddress.Parse(address), port);
            Socket socket = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
            // подключаемся к удаленному хосту
            socket.Connect(ipPoint);
            string message = str; //Console.ReadLine();
            byte[] data = Encoding.Unicode.GetBytes(message);
            socket.Send(data);
        }


        public UserWindow()
        {
            InitializeComponent();

            SqlConnection sqlcon = new SqlConnection(@"Data Source=DESKTOP-DK91BTC\MSSQLSERVER01;Initial Catalog=IPZ;Integrated Security=True");
            sqlcon.Open();
            SqlCommand cmd = new SqlCommand("Select Current_Info.username from Current_Info", sqlcon);
            SqlDataReader dataReader = cmd.ExecuteReader();
            while (dataReader.Read())
            {
                label1.Text = dataReader.GetString(0).ToString();
            }
            sqlcon.Close();
        }

        private void button4_Click(object sender, EventArgs e)
        {
            this.DestroyHandle();
            ConnectToSrv("User '" + label1.Text.Trim() + "' signed out");
            SqlConnection sqlcon = new SqlConnection(@"Data Source=DESKTOP-DK91BTC\MSSQLSERVER01;Initial Catalog=IPZ;Integrated Security=True");
            string query = "Truncate table Current_info";
            SqlDataAdapter sda = new SqlDataAdapter(query, sqlcon);
            DataTable dataTable = new DataTable();
            sda.Fill(dataTable);
        }

        private void button1_Click(object sender, EventArgs e)
        {
           History newForm = new History();
           newForm.Show();
           ConnectToSrv("Show history");
        }

        private void button2_Click(object sender, EventArgs e)
        {
            Balance newForm = new Balance();
            newForm.Show();
            ConnectToSrv("Show balance");
        }

        private void button3_Click(object sender, EventArgs e)
        {
            Cash newForm = new Cash();
            newForm.Show();
            ConnectToSrv("Withdraw cash");
        }

        private void balanceBindingNavigatorSaveItem_Click(object sender, EventArgs e)
        {
            this.Validate();
            this.balanceBindingSource.EndEdit();
            this.tableAdapterManager.UpdateAll(this.dataSet1);

        }

        private void UserWindow_Load(object sender, EventArgs e)
        {
            // TODO: This line of code loads data into the 'dataSet1.Current_Info' table. You can move, or remove it, as needed.
            this.current_InfoTableAdapter.Fill(this.dataSet1.Current_Info);
            // TODO: This line of code loads data into the 'dataSet1.Balance' table. You can move, or remove it, as needed.
            this.balanceTableAdapter.Fill(this.dataSet1.Balance);

        }
    }
}
